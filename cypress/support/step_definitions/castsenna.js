require('cypress-xpath');

/* global Given, When, Then */
const url = 'http://id.cast-soft.com/shop';
const moa_login_url = 'https://id.cast-soft.com/login/login/index/4';

let userEmail = "";
let userPassword = "";
let userFirstName = "";
let userLastName = "";

Given('I open store page', () => {
    cy.viewport( 1920, 1080);
    cy.visit(url);
});

Then('I click on menu options from top-right corner', function () {
    // Verify that 4 products should be displayed
    cy.get('.product-info.py-4', {timeout: 4000}).should('be.visible');

    // Click on navigation button
    cy.get('#navbarDropdown').click();
});

And('I click on SIGN IN', function () {
    // Click on SIGN IN button from dropdown menu
    cy.get('.my-profile .dropdown-item[href*=\'login\']').click();
    cy.url().should('include', '/login/index/4');
});

And('Enter the email: {string} and click on NEXT', function (email) {
    // Fill Email address and click on NEXT
    cy.get('#email').type(email).should('have.value', email);
    cy.get("[type='submit']").click();
});

And('Now enter the password: {string} and click on NEXT', function (password) {
    // Enter the password and click on NEXT
    cy.get('#password').type(password);
    cy.get("[type='submit']").click();
});

And('Check shop page is being open and username is being appeared', function () {
    // Verify user arrives on /shop page
    cy.url().should('include', '/login/login/logincheckfinal.html');
    cy.url().should('include', '/shop/');
    cy.get("[href*='/moa/member']+.nav-profile-text>h6").should('be', 'dev demo');
});

// Test case: Verify valid user can place the order
Then(/^I select "([^"]*)" product on "([^"]*)" page$/, function (productName, pageName) {
    let selectProductButtonLocator;
    if(pageName==="Perpetual")
        selectProductButtonLocator = '//*[@id="tabone"]//*[text()="' + productName + '"]/../../..//*[contains(@class,"btn-primary") and @role="button"]';
    else if(pageName==="Subscription")
        selectProductButtonLocator = '//*[@id="tabtwo"]//*[text()="' + productName + '"]/../../..//*[contains(@class,"btn-primary") and @role="button"]';
    cy.xpath(selectProductButtonLocator).click();
});

And(/^I click on Add to cart button$/, function (arg1) {
    // Click on Add to cart button
    cy.get("#submit_button").click();
    cy.url().should('include', '/shop/cart');
});

And(/^Verify the Order Summary$/, function () {
    // verify product name, quantity and price
    cy.get("h6.pt-1.text-dark").should('be.visible');
    cy.get('#select1').should('have.gte', '1');
    cy.get('#subtotal1').should('be.gt', '0.00');
});

And(/^Click on the CHECKOUT button$/, function () {
    // click on checkout button
    cy.get("[href*='/shop/checkout'] .btn.btn-block.btn-primary.text-uppercase", {timeout: 4000}).click();
});

Then(/^I verify that Product price should be "([^"]*)"$/, function (productPrice) {
    cy.get("#totalprice").should('be.visible').contains(productPrice);
});

Then(/^I verify that Discount should be "([^"]*)"$/, function (discountPrice) {
    cy.get("#appliedcoupon").should('be.visible').contains(discountPrice);
});

Then(/^I verify that Shipping price should be "([^"]*)"$/, function (shippingPrice) {
    cy.get("#shippingPrice").should('be.visible').contains(shippingPrice);
});

Then(/^I verify that TAX should be "([^"]*)"$/, function (tax) {
    cy.get("#tax").should('be.visible').contains(tax);
});

Then(/^I enter Shipping Address FIRST NAME: "([^"]*)",  LAST NAME: "([^"]*)", COMPANY: "([^"]*)", PHONE NUMBER: "([^"]*)", ADDRESS: "([^"]*)", COUNTRY: "([^"]*)", PROVINCE\/STATE: "([^"]*)", CITY: "([^"]*)",  POSTAL \/ ZIP CODE: "([^"]*)"$/, function (firstName, lastName, company, phone, address, country, state, city, zipCode) {
    cy.get('#firstName').clear().type(firstName).should('have.value', firstName);
    cy.get('#lastName').clear().type(lastName).should('have.value', lastName);
    cy.get('#company').clear().type(company).should('have.value', company);
    cy.get('#phone').clear().type(phone).should('have.value', phone);
    cy.get('#address1').clear().type(address).should('have.value', address);
    cy.get('#country').select(country).should('be', country);
    cy.get('#province').clear().type(state).should('have.value', state);
    cy.get('#city').clear().type(city).should('have.value', city);
    cy.get('#zip').clear().type(zipCode).should('have.value', zipCode);
});

Then(/^Enter the below Payment methods FIRST NAME ON CARD: "([^"]*)", LAST NAME ON CARD: "([^"]*)", CREDIT CARD NUMBER: "([^"]*)", EXPIRATION: "([^"]*)", CVV: "([^"]*)"$/, function (ccFirstName, ccLastName, ccNumber, ccExpire, ccCVV ) {
    cy.get("#paymentHeader").click();
    cy.get('#ccFirstName').clear().type(ccFirstName).should('have.value', ccFirstName);
    cy.get('#ccLastName').clear().type(ccLastName).should('have.value', ccLastName);
    cy.get('#ccNumber').clear().type(ccNumber).should('have.value', ccNumber);
    cy.get('#ccExpire').clear().type(ccExpire).should('have.value', ccExpire);
    cy.get('#ccCVV').clear().type(ccCVV).should('have.value', ccCVV);
});

Then(/^Select Billing address Same as Shipping Address$/, function () {
    cy.get("#billingHeader").click();
    cy.get("#same-address1 + label").click();
});

Then(/^Verify the PRODUCTS: "([^"]*)", DISCOUNT: "([^"]*)", SHIPPING: "([^"]*)", and TAX: "([^"]*)" at the Checkout page$/, function (totalprice, appliedcoupon, shippingPrice, tax) {
    cy.get("#totalprice").should('be.visible').contains(totalprice);
    cy.get("#appliedcoupon").should('be.visible').contains(appliedcoupon);
    cy.get("#shippingPrice").should('be.visible').contains(shippingPrice);
    cy.get("#tax").should('be.visible').contains(tax);
});

Then(/^Select 'By clicking 'Confirm Purchase'$/, function () {
    cy.get("#agreement").click();
});

Then(/^Click on CONFIRM PURCHASE$/, function () {
    cy.get("#submitButton").click();
});

Then(/^Verify that order has placed$/, function (callback) {
    //TODO: Need to mock the credit card
});

And(/^I move to Subscription product$/, function () {
    cy.get("[data-target='#tabtwo']").click();
});

And(/^I select (\d+) months subscription$/, function (numberOfMonths) {
    const radioButtonLocator = '//*[contains(text(),"'+numberOfMonths+'") and contains(text(), "onths")]/../../input[@type="radio"]';
    cy.xpath(radioButtonLocator).click({force: true});
});

And(/^I verify saved price is: "([^"]*)" and total price is: "([^"]*)"$/, function (savedPrice, totalPrice) {
    cy.get("#saved").should('be', savedPrice);
    cy.get("#product_price").should('be', totalPrice);
});

Then(/^I verify PRODUCT NAME: "([^"]*)", QANTITY: "([^"]*)" and PRICE: "([^"]*)"$/, function (name, quantity, price) {
    cy.get(".col.d-flex.align-items-center h6").should('be.visible').contains(name);
    cy.get("#select36").should('have.gte', quantity);
    cy.get("#subtotal36").should('be.visible').contains(price);
});

Then(/^I verify SAVE AMOUNT: "([^"]*)" and TOTAL: "([^"]*)"$/, function (saveAmount, totalPrice) {
    cy.get("#saved").should('be.visible').contains(saveAmount);
    cy.get("#totalprice").should('be.visible').contains(totalPrice);
});

Given(/^I move to MOA login page$/, function () {
    cy.viewport( 1920, 1080);
    cy.visit(moa_login_url);
});

Then(/^I click on Create an account link$/, function (callback) {
    cy.get('a.float-right').click();
    cy.get("h4.pl-1").should('be.visible').contains("Create your CAST ID");
});

And(/^I enter FIRST NAME: "([^"]*)", LAST NAME: "([^"]*)", EMAIL: <<randomstring>>@gmail.com, COUNTRY: "([^"]*)", PASSWORD: "([^"]*)"$/, function (fName, lName, country, password) {
    userFirstName = fName;
    userLastName = lName;
    userEmail = userEmail = "testsenna" + Date.now() + "@gmail.com";
    userPassword = password;
    cy.get('#firstName').clear().type(fName).should('have.value', fName);
    cy.get('#lastName').clear().type(lName).should('have.value', lName);
    cy.get('#email').clear().type(userEmail).should('have.value', userEmail);
    cy.get('#country').select(country).should('be', country);
    cy.get('#password').clear().type(userPassword).should('have.value', userPassword);
    cy.get('#confirmpassword').clear().type(userPassword).should('have.value', userPassword);
});

And(/^I check privacy policy and 'I am not robot' checkbox$/, function (callback) {
    cy.get('#privacy+label').click();
    cy.get('#robot+label').click();
});

And(/^I click on Next button for registration$/, function () {
    cy.get("#submitRegistration").click();
    cy.url().should('include', 'shop/checkout');
});

And(/^I verify the user name and email$/, function () {
    cy.get(".bg-white.border-0.d-flex h6.m-0.pt-1 strong").should('be.visible').contains(userFirstName+" "+userLastName);
    cy.get(".bg-white.border-0.d-flex h6.m-0.p-0.text-lowercase").should('be.visible').contains(userEmail);
});

And(/^I do logout from shop$/, function () {
    cy.get("#navbarDropdown").click();
    cy.get("#navbarDropdown + div.dropdown-menu-right a[href*='/shop/logout']").click();
    cy.url().should('include', '/shop/logout');
});

And(/^I do login again with same user$/, function () {
    cy.get('#email').type(userEmail).should('have.value', userEmail);
    cy.get("[type='submit']").click();
    cy.get('#password').type(userPassword);
    cy.get("[type='submit']").click();
});

And(/^I click on "([^"]*)" tab from header$/, function (tabName) {
    const tab = '//li[contains(@class,"nav-item")]//a[contains(text(),"'+tabName+'")]';
    cy.xpath(tab).click({force: true});
});

And(/^I select latest Wysiwyg VERSION: "([^"]*)"$/, function (version) {
    cy.get("#wygRelease").select(version);
    cy.url().should('include', '/wygdownload.html?wygdownloadselect='+version);
});

And(/^I select the first build of download$/, function (callback) {
    const downloadButton = "(//*[contains(@class,'cardsquares-title')]/../*[text()='DOWNLOAD'])[1]";
    cy.xpath(downloadButton).then((anchor) => {
        const url = anchor.prop('href');
        cy.wrap(url).should('include', '.exe');
    });
});

And(/^I click on "([^"]*)" button$/, function (button) {
    const inviteUserButton = "//button[contains(text(),'"+button+"')]";
    cy.xpath(inviteUserButton).click();
});

And(/^I enter FIRST NAME: "([^"]*)", LAST NAME: "([^"]*)" and EMAIL: "([^"]*)"$/, function (fName, lName, email) {
    userFirstName = fName;
    userLastName = lName;
    cy.get("#invitePeople .modal-body input[name='firstname']").type(fName);
    cy.get("#invitePeople .modal-body input[name='lastname']").type(lName);
    cy.get("#invitePeople .modal-body input[name='email']").type(email);
});

And(/^I click on INVITE button on modal$/, function () {
    cy.get("#invitePeople .modal-body button[name='submit']").click();
});

And(/^I search for invited user in search box$/, function () {
    cy.get("#filterInput").type(userFirstName +" "+userLastName);
});

And(/^I verify it is being appeared on search result$/, function () {
    const userTabLocator = "//div[@id='peopleConteiner']//*[@data-name='" + userFirstName + " " + userLastName + "']/following-sibling::h4";
    cy.xpath(userTabLocator).should('be.visible').contains(userFirstName + " " + userLastName);
});

And(/^I click on "([^"]*)" from navigation header tabs$/, function (tabName) {
    cy.xpath("//*[@id='navbarSupportedContent']//*[text()='"+tabName+"']").click();
});

And(/^I select the product for user:"([^"]*)" and I am verifying that dongle code and date is set as per expected$/, function (userName) {
    cy.xpath("//*[text()='"+ userName+"']/../../../*[@class='sorting_1']").as('dongleCodeElement');
    cy.xpath("//*[text()='"+userName+"']/../../../*[@class='sorting_1']/following-sibling::td/span").as('leaseDateElement');

    cy.get("@dongleCodeElement").invoke('text').then((text) => {
        Cypress.env('dongleCode',text);
    });
    cy.get("@leaseDateElement").invoke('text').then((text) => {
        Cypress.env('leaseDate',text);
    });
    // Click on that user
    cy.get("@dongleCodeElement").click();

    // Verify dongle code should match on product list page and extend lease panel
    cy.get("#sideDongleCode").should("be.visible").should("be", Cypress.env('dongleCode'));

    // Verify lease date should match on product list page and extend lease panel
    cy.get("#sideSubscription span").should("be.visible").should("be", Cypress.env('leaseDate'));

    // Click on Extend lease button
    cy.get("#sideButtonGroup [data-target='#extendLease']").click();
});

And(/^I select :"([^"]*)" lease renewal plan$/, function (leaseTime) {
    cy.get('#extendLeaseSelect').select('wysiwyg Design 12 Months Lease Renewal')
});

And(/^I click "([^"]*)" link$/, function (link) {
    cy.get("#sideButtonGroup a[data-target='#extendLease']").click({ force: true });
});

And(/^select lease renewal plan: "([^"]*)"$/, function (leasePlan) {
    cy.get("#extendLeaseSelect").select(leasePlan).should('be', leasePlan);
});

And(/^I check the lease renewal price on EXTEND LEASE dialog and click on CHECKOUT button$/, function () {
    cy.get("#extendLeasePrice").invoke('text').then((text) => {
        Cypress.env('extendLeasePrice',text);
    });

    cy.get("#extendLease button[href='#subTotal']").click();
});

And(/^Last I verify the lease renewal price on checkout dialog$/, function () {
    cy.get("#subTotal #totalPrice").should('be', Cypress.env('extendLeasePrice')).contains(Cypress.env('extendLeasePrice'));
});