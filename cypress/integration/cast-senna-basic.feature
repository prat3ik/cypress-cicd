Feature: Store
  I want to test the basic scenarios for cast senna store page.

  Scenario: Login to store
    Given I open store page
    Then I click on menu options from top-right corner
    And I click on SIGN IN
    And Enter the email: "nuhuhugiy@intempmail.com" and click on NEXT
    And Now enter the password: "Demo@123" and click on NEXT
    And Check shop page is being open and username is being appeared

  Scenario: Verify user can register and login to store
    Given I open store page
    Then I click on menu options from top-right corner
    And I click on SIGN IN
    Then I click on Create an account link
    And I enter FIRST NAME: "Test", LAST NAME: "User", EMAIL: <<randomstring>>@gmail.com, COUNTRY: "India", PASSWORD: "test123"
    And I check privacy policy and 'I am not robot' checkbox
    And I click on Next button for registration
    And I verify the user name and email
    And I do logout from shop
    Then I click on menu options from top-right corner
    And I click on SIGN IN
    And I do login again with same user
    And I verify the user name and email
    And I do logout from shop

  Scenario: Verify valid user can place the order for Perpetual product
    Given I open store page
    Then I select "Report" product on "Perpetual" page
    And I click on Add to cart button
    And Verify the Order Summary
    And Click on the CHECKOUT button
    And Enter the email: "nuhuhugiy@intempmail.com" and click on NEXT
    And Now enter the password: "Demo@123" and click on NEXT
    Then I verify that Product price should be "899.00"
    And I verify that Discount should be "0.00"
    And I verify that Shipping price should be " 0.00"
    And I verify that TAX should be "0.00"
    And I enter Shipping Address FIRST NAME: "dev",  LAST NAME: "demo", COMPANY: "test company name", PHONE NUMBER: "9876543210", ADDRESS: "address", COUNTRY: "India", PROVINCE/STATE: "Gujarat", CITY: "Surat",  POSTAL / ZIP CODE: "394505"
    And Enter the below Payment methods FIRST NAME ON CARD: "dev", LAST NAME ON CARD: "Demo", CREDIT CARD NUMBER: "411111111111", EXPIRATION: "12\/23", CVV: "123"
    And Select Billing address Same as Shipping Address
    And Verify the PRODUCTS: "899.00", DISCOUNT: "0.00", SHIPPING: "0.00", and TAX: "0.00" at the Checkout page
    And Select 'By clicking 'Confirm Purchase'
    And Click on CONFIRM PURCHASE
    And Verify that order has placed

  Scenario: Verify valid user can place the order for Subscription product
    Given I open store page
    Then I move to Subscription product
    And I select "Vivien" product on "Subscription" page
    And I select 6 months subscription
    And I verify saved price is: "397.00" and total price is: "999.00"
    And I click on Add to cart button
    And I verify PRODUCT NAME: "Vivien 6 Months Lease", QANTITY: "1" and PRICE: "599.00"
    And I verify SAVE AMOUNT: "99.00" and TOTAL: "599.00"
    And Click on the CHECKOUT button
    And Enter the email: "nuhuhugiy@intempmail.com" and click on NEXT
    And Now enter the password: "Demo@123" and click on NEXT
    Then I verify that Product price should be "599.00"
    And I verify that Discount should be "0.00"
    And I verify that Shipping price should be " 0.00"
    And I verify that TAX should be "0.00"
    And I enter Shipping Address FIRST NAME: "dev",  LAST NAME: "demo", COMPANY: "test company name", PHONE NUMBER: "9876543210", ADDRESS: "address", COUNTRY: "India", PROVINCE/STATE: "Gujarat", CITY: "Surat",  POSTAL / ZIP CODE: "394505"
    And Enter the below Payment methods FIRST NAME ON CARD: "dev", LAST NAME ON CARD: "Demo", CREDIT CARD NUMBER: "411111111111", EXPIRATION: "12\/23", CVV: "123"
    And Select Billing address Same as Shipping Address
    And Verify the PRODUCTS: "599.00", DISCOUNT: "0.00", SHIPPING: "0.00", and TAX: "0.00" at the Checkout page
    And Select 'By clicking 'Confirm Purchase'
    And Click on CONFIRM PURCHASE
    And Verify that order has placed

#  Scenario: Verify user can extend the lease
#    Given I open store page
#    And I click on "CAST ID" from navigation header tabs
#    And Enter the email: "devpatel.moacast@gmail.com" and click on NEXT
#    And Now enter the password: "1234" and click on NEXT
#    And I select the product for user:"Daniel Vianna" and I am verifying that dongle code and date is set as per expected
#    And I click "EXTEND LEASE" link
#    And I select :"wysiwyg Design 12 Months Lease Renewal" lease renewal plan
#    And I check the lease renewal price on EXTEND LEASE dialog and click on CHECKOUT button
#    And Last I verify the lease renewal price on checkout dialog
#
#  Scenario: Verify user can download the software
#    Given I open store page
#    Then I click on menu options from top-right corner
#    And I click on SIGN IN
#    And Enter the email: "devpatel.moacast@gmail.com" and click on NEXT
#    And Now enter the password: "1234" and click on NEXT
#    And I click on "CAST ID" from navigation header tabs
#    And I click on "downloads" tab from header
#    And I select latest Wysiwyg VERSION: "40"
#    And I select the first build of download
#
#  Scenario: Verify user can invite a new user
#    Given I open store page
#    Then I click on menu options from top-right corner
#    And I click on SIGN IN
#    And Enter the email: "devpatel.moacast@gmail.com" and click on NEXT
#    And Now enter the password: "1234" and click on NEXT
#    And I click on "CAST ID" from navigation header tabs
#    And I click on "people" tab from header
#    And I click on "INVITE NEW USER" button
#    And I enter FIRST NAME: "Test", LAST NAME: "User" and EMAIL: "test3@gmail.com"
#    And I click on INVITE button on modal
#    And I search for invited user in search box
#    And I verify it is being appeared on search result