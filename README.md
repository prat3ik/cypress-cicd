# cypress-cucumber-automation-tests
Automation test cases for Cast Senna project.

# Run tests

_NOTE: Make sure you have installed Cypress globally(`$ npm i -g cypress`)._
```
npm install

# This will execute all tests on CLI only
npm run all-tests-cli 

# This will execute all tests on Cypress window
npm run all-tests-normal 
```  
